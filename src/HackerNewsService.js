const HackerNewsApiUrl = 'https://hacker-news.firebaseio.com/v0';

export async function getNewStories (pageIndex = 0) {
  return getStories('newstories', pageIndex);
}

export async function getTopStories (pageIndex = 0) {
  return getStories('topstories', pageIndex);
}

export async function getBestStories (pageIndex = 0) {
  return getStories('beststories', pageIndex);
}

async function getStories (urlEndPoint, pageIndex) {
  const page = getPageFromPageIndex(pageIndex);
  const topStoriesIdResponse = await fetch(`${HackerNewsApiUrl}/${urlEndPoint}.json`);
  const topStoriesId = await topStoriesIdResponse.json();

  const topStories = await Promise.all(topStoriesId
    .slice(page.from, page.to)
    .map(itemId => getItem(itemId)));

  return topStories;
}

async function getItem (itemId) {
  const itemResponse = await fetch(`https://hacker-news.firebaseio.com/v0/item/${itemId}.json`);
  const item = await itemResponse.json();

  return item;
}

function getPageFromPageIndex (pageIndex) {
  return {
    from: pageIndex * 10,
    to: (pageIndex * 10) + 10
  };
}
