import React from 'react';
import StoriesPage from './StoriesPage';
import { getTopStories } from '../HackerNewsService';

export default function TopStoriesPage () {
  return <StoriesPage getStoriesFrom={getTopStories} />;
}
