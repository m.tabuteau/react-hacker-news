import React from 'react';
import StoriesPage from './StoriesPage';
import { getBestStories } from '../HackerNewsService';

export default function BestStoriesPage () {
  return <StoriesPage getStoriesFrom={getBestStories} />;
}
