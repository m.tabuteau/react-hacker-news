import React from 'react';

export default function StoryLine ({ story }) {
  return (
    <section>
      <h1>
        <a href={story.url}>{ story.title }</a>
      </h1>
      <p>
        { story.score } points by { story.by } | { story.descendants } comments
      </p>
    </section>
  );
}
