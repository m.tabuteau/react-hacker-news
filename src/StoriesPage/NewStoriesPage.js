import React from 'react';
import StoriesPage from './StoriesPage';
import { getNewStories } from '../HackerNewsService';

export default function NewStoriesPage () {
  return <StoriesPage getStoriesFrom={getNewStories} />;
}
