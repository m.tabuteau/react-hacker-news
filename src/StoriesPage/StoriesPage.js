import React from 'react';
import StoryLine from './StoryLine';

class StoriesPage extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      stories: []
    };
  }

  async componentDidMount () {
    const stories = await this.props.getStoriesFrom();
    this.setState({
      stories
    });
  }

  render () {
    const stories = this.state.stories;
    return (
      <ul>
        { stories && stories.map(s => 
          <li>
            <StoryLine story={ s } />
          </li>)
        }
      </ul>
    )
  };
}

export default StoriesPage;
