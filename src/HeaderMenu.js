import React from 'react';
import { NavLink } from 'react-router-dom';

export default function HeaderMenu () {
  return (
    <nav>
      <ul>
        <li>
          <NavLink to='/new' activeClassName='active'>
            new
          </NavLink>
        </li>
        <li>
          <NavLink to='/top' activeClassName='active'>
            top
          </NavLink>
        </li>
        <li>
          <NavLink to='/best' activeClassName='active'>
            best
          </NavLink>
        </li>
      </ul>
    </nav>
  );
}
