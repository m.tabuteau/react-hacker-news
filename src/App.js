import React from 'react';
import { Switch, Route } from 'react-router-dom';
import './App.css';
import HeaderMenu from './HeaderMenu';
import NewStoriesPage from './StoriesPage/NewStoriesPage';
import BestStoriesPage from './StoriesPage/BestStoriesPage';
import TopStoriesPage from './StoriesPage/TopStoriesPage';

export default function App () {
  return (
    <div className='App'>
      <header>
        <HeaderMenu />
      </header>

      <main>
        <Switch>
          <Route path='/top' component={TopStoriesPage} />
          <Route path='/best' component={BestStoriesPage} />
          <Route component={NewStoriesPage} />
        </Switch>
      </main>
    </div>
  );
}
